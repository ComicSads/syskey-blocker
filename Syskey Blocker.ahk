﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

Output=randtxt ; Create a variable of the string "randtxt"
SetTimer, CloseSkey, 500 ; Call Function 'CloseSkey' once every 500 ms

FileInstall, logo.jpg, logo.jpg ; includes logo in compiled exe so splash screen isn't blank
; Splash Screen design
Gui, Add, Picture, x80 y10 w240 h175.2 , logo.jpg 
Gui, Add, Text, x10, Thank you for downloading Syskey Blocker!`rBy default, any attempt to type "syskey" will be replaced with 6 random characters.`rYou can chose a specific string to replace "syskey" by pressing ctrl+shift+k.`rYou can quit the program by clicking the arrow in the bottom right of your screen.`rIt is suggested you move the program exe into your startup folder.
If(A_IsCompiled) ; Check if compiled exe or running source ahk
{
FileDelete, logo.jpg ; If compiled exe, delete the logo file from the computer (to keep things clean)
}
Gui, Cancel ; Close the splash screen window
Gui, Show,, SyskeyBlocker v1.6 ; Title with version number
Return


^+k:: ; ctrl + shift + k triggers code underneath
InputBox, Output, Syskey Blocker, Type what you want syskey to be replaced with.`rType 'randtxt' for random text. ; Create an alert message with a text box to decide what 'Syskey' get's replaced with
Return ; Prevents code underneath being triggered

; * Asterisk makes hotstring trigger without using a key to toggle it, because a scammer wouldn't be trying to activate a hotstring they don't know exists, obviously
; ? Question mark makes hotstring trigger when surrounded with other text
:*?:syskey:: ; When syskey is typed, delete it and run the code
If (Output="randtxt") ; Check if user chose for 'Syskey' to get replaced with random text
{
Output:= ; Empty variable "Output"
Loop,6 ; Loop the commands in the brackets 6 times
{
Random, rand, 1, 26 ; Generate a random number from 1 to 26 (1,2,3,...24,25,26)
rand += 96 ; Add 96 to the random number (1+96=97, 26+96=122)
Letter := Chr(rand) ; Take the new number and figure out what letter it responds to in the ascii character table (97=a, 122=z)
Output := Output . Letter ; Take the letter the random number relates to, and add it to the total string of letters
}
Send, %Output% ; Type all the letters
Output=randtxt ; Set output back to randtxt so 'syskey' will continue to be replaced with random text and not the same string over and over again
} else {
Send, %Output% ; Type the string the user chose
}
Return ; Signifies the end of the hotstring



CloseSkey: ; Function CloseSkey
If (WinExist("Securing the Windows Account Database")) { ; If syskey  window is open
WinClose, Securing the Windows Account Database ; Close sykey window
}
Return ; End Function